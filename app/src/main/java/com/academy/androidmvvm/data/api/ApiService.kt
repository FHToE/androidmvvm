package com.academy.androidmvvm.data.api

import com.academy.androidmvvm.data.model.Comment
import com.academy.androidmvvm.data.model.Post
import com.academy.androidmvvm.data.model.User
import io.reactivex.Single

interface ApiService {
    fun getPosts(): Single<List<Post>>
    fun getCommentsByPostId(id: Int): Single<List<Comment>>
    fun getPostById(id: Int): Single<Post>
    fun getUserById(id: Int): Single<User>
}