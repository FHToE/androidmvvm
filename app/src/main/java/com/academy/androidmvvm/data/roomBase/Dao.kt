package com.academy.androidmvvm.data.roomBase

import androidx.room.Dao
import androidx.room.Query

@Dao
interface PostsDao {
    @Query("SELECT * FROM postentity")
    fun getAll(): List<PostEntity>

    @Query("SELECT * FROM postentity WHERE id = :id")
    fun getById(id: Int): PostEntity
}