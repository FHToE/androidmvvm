package com.academy.androidmvvm.data.api

import com.academy.androidmvvm.data.model.Comment
import com.academy.androidmvvm.data.model.Post
import com.academy.androidmvvm.data.model.User
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Single

class ApiServiceImpl : ApiService {

    override fun getPosts(): Single<List<Post>> {
        return Rx2AndroidNetworking.get("https://jsonplaceholder.typicode.com/posts")
            .build()
            .getObjectListSingle(Post::class.java)
    }

    override fun getCommentsByPostId(id: Int): Single<List<Comment>> {
        return Rx2AndroidNetworking.get("https://jsonplaceholder.typicode.com/posts/${id.toString()}/comments")
            .build()
            .getObjectListSingle(Comment::class.java)
    }

    override fun getPostById(id: Int): Single<Post> {
        return Rx2AndroidNetworking.get("https://jsonplaceholder.typicode.com/posts/${id.toString()}")
            .build()
            .getObjectSingle(Post::class.java)
    }

    override fun getUserById(id: Int): Single<User> {
        return Rx2AndroidNetworking.get("https://jsonplaceholder.typicode.com/users/${id.toString()}")
            .build()
            .getObjectSingle(User::class.java)
    }

}