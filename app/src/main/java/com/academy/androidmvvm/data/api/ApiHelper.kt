package com.academy.androidmvvm.data.api

class ApiHelper(private val apiService: ApiService)  {
    fun getPosts() = apiService.getPosts()
    fun getComments(id: Int) = apiService.getCommentsByPostId(id)
    fun getPost(id: Int) = apiService.getPostById(id)
    fun getUser(id: Int) = apiService.getUserById(id)
}