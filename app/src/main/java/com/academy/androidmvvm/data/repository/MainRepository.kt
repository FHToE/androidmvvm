package com.academy.androidmvvm.data.repository

import com.academy.androidmvvm.data.api.ApiHelper
import com.academy.androidmvvm.data.model.Comment
import com.academy.androidmvvm.data.model.Post
import com.academy.androidmvvm.data.model.User
import io.reactivex.Single

class MainRepository(private val apiHelper: ApiHelper)  {
    fun getPosts(): Single<List<Post>> {
        return apiHelper.getPosts()
    }

    fun getComments(id: Int): Single<List<Comment>> {
        return apiHelper.getComments(id)
    }

    fun getUser(id: Int): Single<User> {
        return apiHelper.getUser(id)
    }

    fun getPost(id: Int): Single<Post> {
        return apiHelper.getPost(id)
    }
}