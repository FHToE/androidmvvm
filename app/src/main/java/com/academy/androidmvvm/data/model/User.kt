package com.academy.androidmvvm.data.model

import com.google.gson.annotations.SerializedName

data class User (
    @SerializedName("id")
    public val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("username")
    val username: String = ""
)