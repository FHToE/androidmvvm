package com.academy.androidmvvm.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}