package com.academy.androidmvvm.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.academy.androidmvvm.data.model.Comment
import com.academy.androidmvvm.data.model.Post
import com.academy.androidmvvm.data.model.User
import com.academy.androidmvvm.data.repository.MainRepository
import com.academy.androidmvvm.utils.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PostViewModel(private val mainRepository: MainRepository, rootPost: Post) : ViewModel() {

    private val post = MutableLiveData<Resource<Post>>()
    private val user = MutableLiveData<Resource<User>>()
    private val comments = MutableLiveData<Resource<List<Comment>>>()
    private val compositeDisposable = CompositeDisposable()

    private val postId: Int = rootPost.id;
    private val userId: Int = rootPost.userId;

    init {
        fetchPost()
        fetchUser()
        fetchComments()
    }

    private fun fetchPost() {
        post.postValue(Resource.loading(null))
        compositeDisposable.add(
            mainRepository.getPost(postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ userList ->
                    post.postValue(Resource.success(userList))
                }, { throwable ->
                    post.postValue(Resource.error("Something Went Wrong", null))
                })
        )
    }

    private fun fetchUser() {
        user.postValue(Resource.loading(null))
        compositeDisposable.add(
            mainRepository.getUser(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ userList ->
                    user.postValue(Resource.success(userList))
                }, { throwable ->
                    user.postValue(Resource.error("Something Went Wrong", null))
                })
        )
    }

    private fun fetchComments() {
        comments.postValue(Resource.loading(null))
        compositeDisposable.add(
            mainRepository.getComments(postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ userList ->
                    comments.postValue(Resource.success(userList))
                }, { throwable ->
                    comments.postValue(Resource.error("Something Went Wrong", null))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getComments(): LiveData<Resource<List<Comment>>> {
        return comments
    }

    fun getUser(): LiveData<Resource<User>> {
        return user
    }

    fun getPost(): LiveData<Resource<Post>> {
        return post
    }

}