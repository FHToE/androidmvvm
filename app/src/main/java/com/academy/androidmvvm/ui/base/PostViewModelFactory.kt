package com.academy.androidmvvm.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.academy.androidmvvm.data.api.ApiHelper
import com.academy.androidmvvm.data.model.Post
import com.academy.androidmvvm.data.repository.MainRepository
import com.academy.androidmvvm.ui.main.viewmodel.PostViewModel

class PostViewModelFactory(private val apiHelper: ApiHelper, private val post: Post) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PostViewModel::class.java)) {
            return PostViewModel(MainRepository(apiHelper), post) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}