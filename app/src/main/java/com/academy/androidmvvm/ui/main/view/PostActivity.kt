package com.academy.androidmvvm.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.academy.androidmvvm.R
import com.academy.androidmvvm.data.api.ApiHelper
import com.academy.androidmvvm.data.api.ApiServiceImpl
import com.academy.androidmvvm.data.model.Comment
import com.academy.androidmvvm.data.model.Post
import com.academy.androidmvvm.data.model.User
import com.academy.androidmvvm.ui.base.PostViewModelFactory
import com.academy.androidmvvm.ui.main.adapter.PostAdapter
import com.academy.androidmvvm.ui.main.viewmodel.PostViewModel
import com.academy.androidmvvm.utils.Status
import kotlinx.android.synthetic.main.activity_main.*

class PostActivity: AppCompatActivity()  {
    private lateinit var postViewModel: PostViewModel
    private lateinit var adapter: PostAdapter
    private lateinit var user: User
    private lateinit var post: Post


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.expandedpost_layout)
        setupUI()
        setupViewModel(post) //? how get post
        setupObserver()
    }

    private fun setupUI() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = PostAdapter(post, user, arrayListOf())
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                (recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerView.adapter = adapter
    }

    private fun setupObserver() {
        postViewModel.getComments().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    it.data?.let { comments -> renderList(comments) }
                    recyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
        postViewModel.getPost().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    it.data?.let { post -> addPost(post) }
                    recyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

        postViewModel.getUser().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    it.data?.let { user -> addUser(user) }
                    recyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun renderList(comments: List<Comment>) {
        adapter.addData(comments)
        adapter.notifyDataSetChanged()
    }

    private fun addUser(user: User) {
        adapter.addUser(user)
        adapter.notifyDataSetChanged()
    }

    private fun addPost(post: Post) {
        adapter.addPost(post)
        adapter.notifyDataSetChanged()
    }

    private fun setupViewModel(post: Post) {
        postViewModel = ViewModelProviders.of(
            this,
            PostViewModelFactory(ApiHelper(ApiServiceImpl()), post)
        ).get(PostViewModel::class.java)
    }

}