package com.academy.androidmvvm.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academy.androidmvvm.R
import com.academy.androidmvvm.data.model.Post
import kotlinx.android.synthetic.main.item_layout.view.*

class MainAdapter(
    private val posts: ArrayList<Post>
) : RecyclerView.Adapter<MainAdapter.DataViewHolder>()  {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(post: Post) {
            itemView.textViewPostTitle .text = post.title
            itemView.textViewPostBody.text = post.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_layout, parent,
                false
            )
        )

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(posts[position])

    fun addData(list: List<Post>) {
        posts.addAll(list)
    }
}