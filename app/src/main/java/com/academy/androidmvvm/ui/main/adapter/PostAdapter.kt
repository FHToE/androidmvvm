package com.academy.androidmvvm.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academy.androidmvvm.R
import com.academy.androidmvvm.data.model.Comment
import com.academy.androidmvvm.data.model.Post
import com.academy.androidmvvm.data.model.User
import kotlinx.android.synthetic.main.expandedpost_layout.view.*

class PostAdapter(private var post: Post, private var user: User, private val comments: ArrayList<Comment>)
    : RecyclerView.Adapter<PostAdapter.DataViewHolder>()  {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(post: Post, user: User, comment: Comment) {
            itemView.postTitle.text = post.title
            itemView.postBody.text = post.body
            itemView.postAuthor.text = user.username
            itemView.commentAuthor.text = comment.name
            itemView.commentBody.text = comment.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PostAdapter.DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_layout, parent,
                false
            )
        )

    override fun getItemCount(): Int = comments.size + 2

    override fun onBindViewHolder(holder: PostAdapter.DataViewHolder, position: Int) =
        holder.bind(post,user,comments[position])

    fun addData(list: List<Comment>) {
        comments.addAll(list)
    }

    fun addPost(post: Post) {
        this.post = post;
    }

    fun addUser(user: User) {
        this.user = user;
    }

}